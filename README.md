>**非常抱歉。
由于不可抗力原因，Paimon Node免费订阅将暂停(或永久)停止更新，在接下来的两周，如果没有另行通知，我们将会彻底停止服务。**

>**感恩有你，Paimon Node有缘再见。**

<div align="center"><img align=center src="https://github.com/paimonhub/Paimonnode/raw/main/images/logo.png" width=250></div>

<div align="center">
<h1>Paimon Node</h1>

 [![Telegram Channel][tg-svg]][tg-chan]<br>
</div>





[tg-chan]: https://t.me/nodpai
[tg-svg]: https://img.shields.io/badge/Telegram-@nodpai-blue.svg?style=plastic

